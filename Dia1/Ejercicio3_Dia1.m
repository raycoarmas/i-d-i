function [ output_args ] = Ejercicio3_Dia1( input_args )
%EJERCICIO3_DIA1 Summary of this function goes here
%   Detailed explanation goes here

%calcular el histograma de los valores de la matriz paa los siguientes
%intervalos [0,0.3),[0.3,0.5),[0.5,0.6),[0.6,0.7),[0.7,0.9] y
%[0.9,1)
 clear;
    load('matrizAleatoriaCienMilPorMil.mat');
    %interval = [0 0.3 0.5 0.6 0.7 0.9 1];
    interval = [0 0.3 0.5 0.6 0.7 0.9 1];
    result = [];
    tic;
   
    for(i = 1: size(interval,2)-1  )
    	result = [result size(find(x >= interval(i) & x < interval(i+1)),1) ];
    end
    bar (result)
    
    toc;
    
    
    clear()