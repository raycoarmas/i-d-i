function [ output_args ] = Untitled1( input_args )
%UNTITLED1 Summary of this function goes here
%   Detailed explanation goes here
    
    clear()
    x1 = -3.3:1:-1.3;
    x2 = -2:1:2;
    x3 = -0.1:0.001:0.1;
    
    tic
    subplot(2,2,1);
    plot(x1,fraiz(x1),'r');
    
    subplot(2,2,2);
    plot(x2,fseno(x2),'r');
    
    subplot(2,2,3);
    plot(x3,fseno(x3),'r');
    
    toc
    clear()
end

function y = fraiz(x)
    y = sqrt(x.^2-1);
end

function y = fseno(x)
    y = x.^2.*sin(1./x);
end
