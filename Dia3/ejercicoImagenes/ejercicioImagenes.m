function [ output_args ] = ejercicioImagenes( input_args )
%EJERCICIOIMAGENES Summary of this function goes here
%   Detailed explanation goes here
%todas las capturas son CAPTURA cargar y guardar en una varaible

    clear()
    load('cap1.mat');
    cap1 = captura;
    load('cap2.mat');
    cap2 = captura;
    load('cap3.mat');
    cap3 = captura;
    load('cap4.mat');
    cap4 = captura;
    load('cap5.mat');
    cap5 = captura;
    load('cap6.mat');
    cap6 = captura;
    load('cap7.mat');
    cap7 = captura;
    load('cap8.mat');
    cap8 = captura;
    
    imagen1.r = cap1(:,:,1);
    imagen1.g = cap1(:,:,2);
    imagen1.b = cap1(:,:,3);
    
    imagen2.r = cap2(:,:,1);
    imagen2.g = cap2(:,:,2);
    imagen2.b = cap2(:,:,3);
    
    imagen3.r = cap3(:,:,1);
    imagen3.g = cap3(:,:,2);
    imagen3.b = cap3(:,:,3);
    
    imagen3.r = cap3(:,:,1);
    imagen3.g = cap3(:,:,2);
    imagen3.b = cap3(:,:,3);
    
    imagen4.r = cap4(:,:,1);
    imagen4.g = cap4(:,:,2);
    imagen4.b = cap4(:,:,3);
    
    imagen5.r = cap5(:,:,1);
    imagen5.g = cap5(:,:,2);
    imagen5.b = cap5(:,:,3);
    
    imagen6.r = cap6(:,:,1);
    imagen6.g = cap6(:,:,2);
    imagen6.b = cap6(:,:,3);
    
    dif12 = DiferenciaImagen(cap1,cap2);
    dif23 = DiferenciaImagen(cap2,cap3);
    dif34 = DiferenciaImagen(cap3,cap4);
    dif45 = DiferenciaImagen(cap4,cap5);
    dif56 = DiferenciaImagen(cap5,cap6);
    
    %image(cap6(:,:,1)-cap1(:,:,1));%diferencias entre la 1 y 6
    image(dif12+dif23+dif34+dif45+dif56)
    clear();
end

function y = DiferenciaImagen(img1, img2)
    y = img2-img1;
end
    