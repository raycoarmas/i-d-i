function [ output_args ] = cargarImagenes( input_args )
%CARGARIMAGENES Summary of this function goes here
%   Detailed explanation goes here
    %imag = [;;;];
    dif.r = [];
    dif.g = [];
    dif.b = [];
    
    for(i = 1: 8  )
        filename = ['cap' num2str(i) '.mat'];
        capAux = cargar(filename);
        cap(i).r = capAux(:,:,1);
        cap(i).g = capAux(:,:,2);
        cap(i).b = capAux(:,:,3);
    end
    
    for(i = 1: 8  )
       dif.r = [dif.r mean(mean(cap(i).r(200:400,400:700)))];
       dif.g = [dif.g mean(mean(cap(i).g(200:400,400:700)))];
       dif.b = [dif.b mean(mean(cap(i).b(200:400,400:700)))];
    end
    plot(dif.r, 'r');
    hold on;
    plot(dif.g, 'g');
    plot(dif.b, 'b');
end

function y = cargar(filename)
   load(filename);
   y = captura;
end