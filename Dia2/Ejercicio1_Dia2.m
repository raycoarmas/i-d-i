function [ output_args ] = Ejercicio1_Dia2( input_args )
%EJERCICIO1_DIA2 Summary of this function goes here
%   Detailed explanation goes here
clear()
% f(x)= x^3 - x +1 -3 <= x <= 3 paso = 0.1
x1 = -3:0.1:3;
x2 = -5:0.05:5;
x3 = -10:0.01:10;
x4 = -pi:0.01:pi;
tic

subplot(2,2,1);
plot(x1,f(x1),'r');

subplot(2,2,2);
plot(x2,fDivAbs(x2),'r');

subplot(2,2,3);
plot(x3,fDiv(x3),'r');

subplot(2,2,4);
plot(x4,fsen(x4),'r')
toc

clear()
end
function y = f(x)
   y = x.^3 - x +1;
end   
function y = fDivAbs(x)
  y = 1./(1+abs(x).^2);
end  
function y = fDiv(x)  
  y = x.^2./(x.^2-1);
end
function y =  fsen(x)
  y = sin(exp(x));
end