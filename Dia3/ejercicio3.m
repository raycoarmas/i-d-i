function [ output_args ] = ejercicio3( input_args )
%EJERCICIO3 Summary of this function goes here
%   Detailed explanation goes here
    %toro (donut)
    clear()
    R1 = 3;
    R2 = 1;
    Nt = 20;
    Np = 50;
    theta = linspace(0,2*pi,Nt);%quitando el 2 consigues la capa superior
    phi = linspace(0,2*pi,Np);% quitando el 2 se consigue la mitad delantera
    [Theta, Phi] = meshgrid(theta,phi);
    X = (R1+R2.*cos(Theta)).*cos(Phi);
    Y = (R1+R2.*cos(Theta)).*sin(Phi);
    Z = R2.*sin(Theta);
    surf(X,Y,Z);
    axis equal;
    axis off;
    cameratoolbar;