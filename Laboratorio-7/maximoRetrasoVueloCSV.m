function [] = maximoRetrasoVueloCSV()
%MAXIMORETRASOVUELOCSV Summary of this function goes here
%   Detailed explanation goes here
    clear();

    ds = datastore('airlinesmall.csv', 'TreatAsMissing', 'NA'); 
    ds.SelectedFormats{strcmp(ds.SelectedVariableNames, 'TailNum')} = '%s'; 
    ds.SelectedFormats{strcmp(ds.SelectedVariableNames, 'CancellationCode')} = '%s'; 
    dsPreview = preview(ds); 
    dsPreview(:,{'AirTime','TaxiIn','TailNum','CancellationCode'})
    
    subset = [];
    while hasdata(ds) 
        t = read(ds); 
        t = t(strcmp(t.UniqueCarrier, 'UA') & strcmp(t.Origin, 'BOS'), :); 
        subset = vertcat(subset, t); 
    end
    subset(1:10,[9,10,15:17]);


    reset(ds); 
    ds.SelectedVariableNames = {'ArrDelay'};
    
    result = mapreduce(ds, @maxTimeMapper, @maxTimeReducer)
    readall(result)
    
end

function maxTimeMapper(data, ~, intermKVStore) % Copyright 2014 The MathWorks, Inc.
    maxArrDelay = max(data{:,:});
    add(intermKVStore, 'ArrDelay',maxArrDelay); 
end

function maxTimeReducer(~, intermValsIter, outKVStore) % Copyright 2014 The MathWorks, Inc.
    maxArrDelay = -inf; 
    while hasnext(intermValsIter) 
        maxArrDelay = max(maxArrDelay, getnext(intermValsIter)); 
    end
    add(outKVStore, 'ArrDelay', maxArrDelay); 
end


