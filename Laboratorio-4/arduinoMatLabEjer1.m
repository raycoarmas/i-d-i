function [ output_args ] = arduinoMatLabEjer1( numero_muestras )
%ARDUINOMATLABEJER1 Summary of this function goes here
%   Detailed explanation goes here
    close all;
	clc;
	y=zeros(1,1000); %Vector donde se guardar�n los datos
	alarmas = 0;

	%Inicializo el puerto serial que utilizar�
	delete(instrfind({'Port'},{'COM3'}));
	puerto_serial=serial('COM3');
	puerto_serial.BaudRate=9600;
	warning('off','MATLAB:serial:fscanf:unsuccessfulRead');
	

	%Abro el puerto serial
	fopen(puerto_serial); 
	

	%Declaro un contador del n�mero de muestras ya tomadas
    contador_muestras=1;
	
    channelID = 629232;
    writeKey = 'EFAXSJQQAGWXCVDH';
    

	%Bucle while para que tome y dibuje las muestras que queremos
	while contador_muestras<=numero_muestras
	        valor_potenciometro=fscanf(puerto_serial,'%d')';
	        y(contador_muestras)=(valor_potenciometro(1))*5/1024;
            if(y(contador_muestras) > 2)
                disp('Cuidado valor muy alto');
                alarmas = alarmas + 1;
                webwrite('https://api.thingspeak.com/apps/thingtweet/1/statuses/update', 'api_key', 'ZN00ITMHT89RWV73', 'status', 'Publicaci�n en caso de alarma...' );
            end
	        hist(y)      
	        drawnow
	        contador_muestras=contador_muestras+1;
            thingSpeakWrite(channelID,'Fields',[1,2],'Values',{y(contador_muestras),alarmas},'WriteKey',writeKey);
            pause(15);
	end
	max(y)%obtencion del maximo
%Cierro la conexi�n con el puerto serial y elimino las variables
	fclose(puerto_serial); 
	delete(puerto_serial);
	clear all;
	

	end
