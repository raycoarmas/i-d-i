function [ output_args ] = buenaspracticas( input_args )
%BUENASPRACTICAS Summary of this function goes here
%   Detailed explanation goes here
clear();

x = eye([4,4]);
y = sparse(x);%la matriz escasa ocupa menos espacio
whos;

disp('x * 2');
tic
x*2;
toc

disp('y * 2');
tic 
y*2;
toc

clear();

disp('main1');
tic
main1();
whos;
toc

clear();
disp('main2');
tic
main2();
whos;
toc

end
%los whos fuera de las funciones no ven lo que hay dentro.
function main1
    x = 5;
    nestfun1
    function nestfun1
        x = x + 1;
    end
end

function main2
    nestfun2
    function nestfun2
        x = 5;
    end
    x = x + 1;
    
end
