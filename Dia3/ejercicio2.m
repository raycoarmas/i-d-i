function [ output_args ] = ejercicio2( input_args )
%EJERCICIO2 Summary of this function goes here
%   Detailed explanation goes here
    %esfera
    clear();

    R = 1;
    Nt = 20;
    Np = 40;
    theta = linspace(0,pi,Nt);%diviendo entre 2 pi se consigue media esfera
    phi = linspace(0,2*pi,Np);% quitando el 2 se consigue media esfera
    [Theta, Phi] = meshgrid(theta,phi);
    X = R.*sin(Theta).*cos(Phi);
    Y = R.*sin(Theta).*sin(Phi);
    Z = R.*cos(Theta);
    surf(X,Y,Z);
    axis equal;
    axis off;
    cameratoolbar;